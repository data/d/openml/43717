# OpenML dataset: Consumer-Price-Index-in-Denver-CO

https://www.openml.org/d/43717

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context:
The Consumer Price Indexes (CPI) program produces monthly data on changes in the prices paid by urban consumers for a representative basket of goods and services. It is a useful way to compare changes in the economy across time.
Content:
This data covers Jan 1913-May 2017, and is normalized to CPI-U all items 1982-84=100, not seasonally adjusted. Fields include time of measurement and CPI score.
Acknowledgements:
This dataset was compiled on behalf of the Bureau of Labor Statistics (BLS) via Colorado Department of Labor  Employment (CDLE) and hosted on data.colorado.gov.
Inspiration:

What periods of time have seen the highest/lowest CPI? 
When has inflation been the worse?
Can you predict present CPI?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43717) of an [OpenML dataset](https://www.openml.org/d/43717). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43717/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43717/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43717/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

